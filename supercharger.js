//
//  supercharger.js
//  Supercharger.js
//
//  Created by Oliver Bredemeyer on 24/06/2014.
//  Copyright (c) 2014 Oliver Bredemeyer. All rights reserved.
//

(function(){

// "use strict";

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - -  NAMESPACES - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
var _statics = {},
	_classes = {};



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - -  STATIC METHODS - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

// Sup.validateObject (Object,Object/pattern) -> ValidationResult
_statics.validateObject = function (obj,pattern) {
	// pattern: {property:[type:Object,validator:Function]}
	// #todo clean up if/else?
	var valid = [],
		invalid = [],
		missing = [];

	Object.keys(pattern).forEach(function(key){

		if (obj.hasOwnProperty(key) === true) {

			if (obj[key].isInstanceOf(pattern[key][0]) === true) {

				if (typeof pattern[key][1] === 'function') {
					(pattern[key][1](obj[key]) === true ? valid : invalid).push(key);
				} else {
					valid.push(key);
				}

			} else {
				invalid.push(key);
			}
			
		} else {
			missing.push(key)
		}

	});

	var result = new _classes.ValidationResult({
		obj:obj,
		pattern:pattern,
		valid:valid,
		invalid:invalid,
		missing:missing
	});

	return result;

};


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - -  Object - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

// ===========
// INHERITANCE
// ===========

// Object::objectByPrototyping () -> Object
Object.prototype.objectByPrototyping = function () {
	var cons = function () {};
	cons.prototype = this;
	var newobj = new cons();
	return newobj;
};

// Object::objectByDuplication () -> Object
Object.prototype.objectByDuplication = function (depth) {
	depth = depth || 'deep';

	var newobj = this.objectByPrototyping();

	if (depth == 'shallow') {
		for (var key in this) {
			if (this.hasOwnProperty(key)) {
				newobj[key] = this[key];
			}
		}
	} else if (depth == 'deep') {
		for (var key in this) {
			if (this.hasOwnProperty(key)) {
				if (typeof this[key] == 'object') {
					newobj[key] = this[key].objectByDuplication();
				} else {
					newobj[key] = this[key];
				}
			}
		}
	}

	return newobj;
};

// Object::objectByFusion (Object) -> Object
Object.prototype.objectByFusion = function (obj) {
	// NOTE: References to objects as properties do
	// not carry back to obj, but to a new object
	// dupobj.
	// #TODO: add shallow fusion?

	obj = obj || {};

	var newobj = this.objectByDuplication('deep');
	var dupobj = obj.objectByDuplication('deep');

	for (var key in dupobj) {
		if (dupobj.hasOwnProperty(key)) {
			newobj[key] = dupobj[key];
		}
	}

	return newobj;
};

// Object::extendPrototypeTo (Function/constructor) -> ()
Object.prototype.extendPrototypeTo = function (child) {
	var F = function () {};
	F.prototype = this.prototype;
	child.prototype = new F();
	child.prototype.constructor = child;
	child.uber = this;
	return true;
};


// =============
// OTHER METHODS
// =============

// Object::isInstanceOf (Function/constructor) -> Boolean
Object.prototype.isInstanceOf = function (obj) {
	return this instanceof obj;
};

// Object::fillPropertiesFrom (Object,Boolean?) -> Object/self
Object.prototype.fillPropertiesFrom = function (obj,overwrite) {
	if (arguments.length == 1) { overwrite = true; }

	var _this = this,
		copyProperty;

	switch (overwrite) {
		case true:
			copyProperty = function(str){
				_this[str] = obj[str];
			}
			break;

		case false:
			copyProperty = function(str){
				if (!_this.hasOwnProperty(str)) {
					_this[str] = obj[str];
				}
			}
			break;
	}

	Object.keys(obj).forEach(copyProperty);
	return this;
};



// =============
// CLASS METHODS
// =============

// Object.properties (Object) -> Array[String]
Object.properties = function (obj) {
	var properties = Object.keys(obj).filter(function(str){
		return typeof obj[str] !== 'function';
	});

	return properties;
};

// Object.methods (Object) -> Array[String]
Object.methods = function (obj) {
	var methods = Object.keys(obj).filter(function(str){
		return typeof obj[str] === 'function';
	});

	return methods;
};

// Object.values (Object) -> Array[Any]
Object.values = function (obj) {
	return Object.keys(obj).map(function(key){
		return obj[key];
	});
};



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - -  Function - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

// Function::setTypecheck (Object/pattern) -> Function/self
Function.prototype.setTypecheck = function (pattern) {
	this.argumentPattern = pattern;
	return this;
};

// Function::setArgRemap (Function) -> Function/self
Function.prototype.setArgRemap = function (fnc) {
	// fnc: Function (Array/arguments) -> Object
	this.argRemap = fnc;
	return this;
};

// Function::setDefaultArgs (Object) -> Function/self
Function.prototype.setDefaultArgs = function (obj) {
	this.defaultArgs = obj;
	return this;
};

// Function::prepareArgs (Object/arguments) -> Object
Function.prototype.prepareArgs = function (args) {
	var params;

	if (!(args.length === 1 && typeof args[0] === "object")) {
		params = this.argRemap(Array.prototype.slice.call(args));
	} else {
		params = args[0];
	}

	var validation = _statics.validateObject(params,this.argumentPattern);
	if (validation.allPropertiesValid === false) {
		console.log(validation);
		throw new Error('Function::prepareArgs - invalid or missing arguments');
		return false;
	}

	return params;
};



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - -  String - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

// String::format (Object|Function) -> String
String.prototype.format = function (obj) {
	var str = this.valueOf(),
		tmpstr = str,
		fmtstr,
		objrx = /%\((\w+)\)/g,
		result,
		valueFor;

	switch (typeof obj) {

		case 'object':
		valueFor = function (key) {
			return obj[key];
		};
		break;

		case 'function':
		valueFor = function (str) {
			return obj(str);
		};
		break;

		default:
		valueFor = function (str) {
			return window[str];
		};

	}

	while (result = objrx.exec(str)) {
		tmpstr = tmpstr.replace(result[0],valueFor(result[1]));
	}

	fmtstr = tmpstr;
	return fmtstr;
};



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - -  Number - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

// Number::isInteger () -> Boolean
Number.prototype.isInteger = function () {
	return this.valueOf() % 1 == 0;
};



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - -  CUSTOM CLASSES - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

// ==================
// Object::Restricted
// ==================
// @constructor Object::Restricted (Object/interface) -> Object
Object.prototype.Restricted = function (interfacedesc) {
	/*interfacedesc: Object {
		getters:Array[String],
		setters:Array[String],
		methods:Array[String]
	}*/
	var obj_interface = {};

	// typecheck
	if (_statics.validateObject(interfacedesc,{
		getters:[Array],
		setters:[Array],
		methods:[Array]
	}) === false) {
		throw new Error('Object::Restricted - invalid interface descriptor');
	}

	var _obj = this;

	var _defaultGetters = [],
		_defaultSetters = [],
		_defaultMethods = ['isInstanceOf'];

	interfacedesc.getters.concat(_defaultGetters).forEach(function(str){
		var gettername = 'get' + str.charAt(0).toUpperCase() + str.slice(1);
		obj_interface[gettername] = function () {
			return _obj[str];
		}
	});

	interfacedesc.setters.concat(_defaultSetters).forEach(function(str){
		var settername = 'set' + str.charAt(0).toUpperCase() + str.slice(1);
		obj_interface[settername] = function (any) {
			_obj[str] = any;
		};
	});

	interfacedesc.methods.concat(_defaultMethods).forEach(function(str){
		obj_interface[str] = function() {
			var args_arr = Array.prototype.slice.call(arguments)
			return _obj[str].apply(_obj,args_arr);
		}
	});

	return obj_interface;
};


// ==========================
// ValidationResult (PRIVATE)
// ==========================
// @constructor ValidationResult (Object/details) -> ValidationResult
_classes.ValidationResult = function (details) {
	// @warning no typecheck
	/*details: {
		obj:Object,
		pattern:Object/pattern
		valid:Array[String],
		invalid:Array[String],
		missing:Array[String],
		fillAttempted?:Boolean
	}*/

	this.obj = details.obj;
	this.pattern = details.pattern;
	this.valid = details.valid;
	this.invalid = details.invalid;
	this.missing = details.missing;

	this.computeValidationResults();

	this.fillAttempted = details.hasOwnProperty('fillAttempted') ? details.fillAttempted : false;

};

// ValidationResult::attemptFillWithDefaults (defaults?) -> ValidationResult/self
_classes.ValidationResult.prototype.attemptFillWithDefaults = function (defaults) {
	var _defaults;

	// return if successful initial validation
	if (this.allPropertiesValid === true) {
		return this;
	}

	// eject if invalid data
	if (this.hasInvalidProperties === true) {
		this.fillAttempted = true;
		return this;
	}

	// fill
	if (arguments.length !== 0) {
		_defaults = defaults;
	} else {
		if (this.obj.constructor.hasOwnProperty('defaults')) {
			_defaults = this.obj.constructor.defaultArgs;
		} else {
			return this;
		}
	}

	var key,
	still_missing = [];
	while (key = this.missing.pop()) {
		if (_defaults.hasOwnProperty(key)) {
			this.obj[key] = _defaults[key];
			this.valid.push(key)
		} else {
			this.still_missing.push(key);
		}
	}

	// set new data
	this.missing = still_missing;
	this.computeValidationResults();

	return this;

};

// ValidationResult::computeValidationResults () -> ()
_classes.ValidationResult.prototype.computeValidationResults = function () {
	this.hasMissingProperties = (this.missing.length !== 0);
	this.hasInvalidProperties = (this.invalid.length !== 0);
	this.allPropertiesValid = (this.invalid.length == 0 && this.missing.length == 0) ? true : false;
};



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - INTERFACE - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
var sc_interface = {
	validateObject:_statics.validateObject
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - -  EXPORT - - - - - - - - - - - - - - - //
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //
window.Supercharger = sc_interface;
window.Sup = window.Supercharger;

})();
