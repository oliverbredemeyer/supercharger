# Supercharger.js Reference
DRAFT VERSION - INCOMPLETE, MAY CONTAIN ERRORS



## Static methods

### Data validation with Sup.validateObject
Type signature: `(Object,Object/pattern) -> Sup.ValidationResult`

This accepts a pattern object with the same property names as the object to validate, and their corresponding values as an `Array` of the form `[constructor,validator]`, where:

* `constructor` is its constructor (primitives of type `string`,`number` and `boolean` are converted to corresponding objects automatically)
* `validator` is a function with type signature `(Object) -> Boolean` accepting the actual value of the property as its argument.

`Sup.validateObject` returns an instance of `Sup.ValidationResult`, and a boolean value for validation success or failure is stored in the latter's `allPropertiesValid` property.

#### Usage
Success/failure checking

	var pattern = {
		foo:[String],
		bar:[Number, function(num){return num.isInteger();} ]
	};

	Sup.validateObject({ foo:"bat" , bar:5 }).allPropertiesValid // true
	Sup.validateObject({ foo:"bat" , bar:5.6}).allPropertiesValid // false
	Sup.validateObject({ foo:5 }).allPropertiesValid // false

Attempting to fill incomplete data (assumes previous example)

	var defaults = {
		foo:"default",
		bar:42
	};

	Sup.validateObject({ foo:"bat" }).attemptFillWithDefaults(defaults).obj // { foo:"bat" , bar:42 }
	Sup.validateObject({ foo:"bat" , bar:1.5 }).attemptFillWithDefaults(defaults).allPropertiesValid // false



## Additions to Object.prototype

### Inheritance methods
Supercharger provides methods for implementing inheritance both by altering the prototype of an object constructor and by creating new objects which inherit properties directly from existing objects.


`Object::objectByPrototyping()` returns an object with no own properties and `this` as its prototype. Type signature: `() -> Object`

`Object::objectByDuplication(depth)` returns a new object with all properties of `this`, where `depth` is either `'deep'` or `'shallow'`. Shallow depth will preserve references to objects as properties of `this`, where deep will return an object with references to duplicate objects. Type signature: `(String) -> Object`

`Object::objectByFusion(obj)` returns a new object which is a duplicate of `this`, preserving its prototype, and inherits all own properties of `obj` in preference to own properties of `this`.

`Object::extendPrototypeTo(cns)`, where `cns` is a constructor, sets the prototype of `cns` to have the properties of the prototype of `this`, allowing for modification by using an object with no own properties but `this.prototype` as the prototype of `cns`. Type signature: `(Function/constructor) -> ()`

### Instance checking with Object::isInstanceOf
Type signature: `(Function/constructor) -> Boolean`

Same purpose as the `instanceof` operator, except eliminates the need to use `typeof` as well when dealing with primitives as they will be converted to objects.

#### Usage

	"Foo" instanceof String // false
	new String("Foo") instanceof String //true
	typeof "Foo" === 'string' //true
	typeof new String("Foo") === 'string' //false
	
	"Foo".isInstanceOf(String) // true
	new String("Foo").isInstanceOf(String) // true

### Filling properties with Object::fillPropertiesFrom
Type signature: `(Object,Boolean?) -> Object/self`

`Object::fillPropertiesFrom(obj,overwrite)` returns `this`, with own properties copied (objects referenced directly) from `obj`. If `overwrite` is `true`, properties of `obj` take precedence; if `false`, properties of `this` take precedence.

#### Usage
	{ foo:'a' , bar:'b' }.fillPropertiesFrom({ bar:1 , bat:2 }) // { foo:'a' , bar:'b' , bat:2 }
	{ foo:'a' , bar:'b' }.fillPropertiesFrom({ bar:1 , bat:2 }, true) // { foo:'a' , bar:1 , bat:2 }

To avoid references to objects carrying through from `obj`:

	thing.fillPropertiesFrom(obj).objectByDuplication()



## Additions to static methods of Object

### Fetching properties with Object.properties and Object.methods
`Object.properties(obj)` returns an array of the keys of properties of `obj`. Type signature: `(Object) -> Array[String]`

`Object.methods(obj)` returns an array of the keys of methods of `obj`. Type signature: `(Object) -> Array[String]`

### Fetching values with Object.values
`Object.values(obj)` returns an array of the values corresponding to the keys in `obj`. This may be used to simulate ES6-style for-of loop functionality. Type signature: `(Object) -> Array[any]`



## Additions to Function.prototype
The use of named parameters of a function in the form of an object passed to the function as its single argument is encouraged. Supercharger.js facilitates typechecking and validation of arguments and remapping of arguments to their named counterparts in functions using methods which can be chained onto a function declaration statement as each returns the function itself. These are as follows:

* `Function::setTypecheck` accepts a pattern of the form used in `Sup.validateObject`. This sets the property `Function::argumentPattern`, which is used within `Function::prepareArgs`. Type signature: `(Object/pattern) -> Function/self`
* `Function::setArgRemap` accepts a function of type signature `(Array) -> Object`, which will receive the arguments when called without using a single-object format, and remap them to conform to the standard format for arguments. This is called only when necessary from `Function::prepareArgs`. Type signature: `(Function (Array) -> Object) -> Function/self`
* `Function::setDefaultArgs` sets default optional arguments for when input has missing properties. Automatic fill not yet implemented.

The first statement of a function written using this format should call its own `prepareArgs` method on its arguments, for example `this.constructor.prepareArgs(arguments);`.

### Usage
The following class constructor:

	var Person = function (name,age) {
		var _name,
			_age;

		// Check argument format
		if (arguments.length == 1 && typeof arguments[0] == "object") {
			_name = name.name;
			_age = name.age;
		}

		// typecheck
		if !(
				(typeof _name == "string" || _name instanceof String)
				&& (typeof _age == "number" || _age instanceof Number)
				&& (_age % 1 == 0)
			) {
			throw new Error("Person: invalid arguments supplied");
		}

		// add properties
		this.name = _name;
		this.age = _age;
	};

can be written as follows using Supercharger.js:

	var Person = function () {
		var params = this.constructor.prepareArgs(arguments);
		this.name = params.name;
		this.age = params.age;
	}.setTypecheck({
		name:[String],
		age:[Number,function(num){return num.isInteger();}]
	}).setArgRemap(function(args){
		return {
			name:args[0],
			age:args[1]
		};
	});

Both of these can be invoked using either `new Person({ name:foo , age:bar })` or `new Person(foo,bar)` due to the remapping of unnamed arguments to the former format, which is handled automatically by Supercharger.js when necessary using the `argRemap` set at declaration.



## Additions to String.prototype

### String formatting with String::format
Type signature: `(Object|Function?) -> String`

`String::format` is Supercharger.js' string formatting method. As its single argument it accepts either an object, from which properties are retrieved, or a function which will return a suitable string for a string argument passed to it. If no argument is passed, variables will be retrieved from the method's own scope.

The format specifier `%(name)` will insert the value of the property with key `name` of the object passed to `String::format` or the return value of the function passed to it with argument `name`, or if `String::format` is called with no arguments, the contents of the variable `name` in global scope.


#### Usage
In global scope:

	var name = "John Smith",
		age = 42;

	"Hi, I'm %(name) and I'm %(age) years old.".format() // "Hi, I'm John Smith and I'm 42 years old."

Passing an object:

	var data = { name:"John Smith" , age:42 };

	"Hi, I'm %(name) and I'm %(age) years old.".format(data) // "Hi, I'm John Smith and I'm 42 years old."

Using a closure to pass scope variables (**not recommended**):

	(function(){
		var scope = function (str) {
			return eval(str);
		};

		var name = "John Smith",
			age = 42;

		"Hi, I'm %(name) and I'm %(age) years old.".format(scope) // "Hi, I'm John Smith and I'm 42 years old."

	})();



## Additions to Number.prototype
### Checking integers with Number::isInteger
Type signature: `() -> Boolean`

Returns the result of `this.valueOf() % 1 == 0`.



## Custom classes
### Object::Restricted
Type signature: `({ getters:Array[String] , setters:Array[String] , methods:Array[String] }) -> Object`

This allows use of immutable and private properties and methods by creating a public interface object. Chain method to return value of a constructor; the initial object is made unretrievable.

The arguments passed to `Object::Restricted` should be:

* `getters`: An array of the property names as strings of the object which can be retrieved via the public interface. The property `name` corresponds to a method `getName` of type signature `() -> any` in the interface object.
* `setters`: An array of the property names as strings of the object which can be set via the public interface. The property `name` corresponds to a method `setName` of type signature `(any) -> ()` in the interface object.
* `methods`: An array of the method names as strings which can be called via the public interface. Methods keep their names and arguments are passed to the original object.

#### Usage
Creating a restricted object:

	var str = new String("hello").Restricted({
		getters:['length'],
		setters:[],
		methods:['concat']
	});

	str.getLength() // 5
	str.toString() // "[object Object]"
	str.concat(" there") // "hello there"
	str.slice() // TypeError


### ValidationResult (private)
An instance of this is returned by `Sup.validateObject`.

Properties:

* `valid : Array[String]` all properties which cleared both instance check and custom validation if applicable.
* `invalid : Array[String]` all properties which failed instance check or custom validation. Cannot be filled by `ValidationResult::attemptFillWithDefaults` as invalid data has been supplied.
* `missing : Array[String]` all properties of pattern which were not present in object. If not critical, can be filled by `ValidationResult::attemptFillWithDefaults`.
* `allPropertiesValid : Boolean` true if no missing or invalid properties, false otherwise.
* `hasMissingProperties : Boolean` true if properties missing, false otherwise.
* `hasInvalidProperties : Boolean` true if properties invalid, false otherwise.

Methods:

* `attemptFillWithDefaults : (defaults?:Object) -> ValidationResult/self` copies missing properties from defaults object, or if not given, `defaults` property of the object's constructor. Updates validation results (valid,invalid,missing) and returns `this`.
* `computeValidationResults : () -> ()` does *not* revalidate object but updates all computed properties.